= DAS Administration Review Instructions
:team: Administration
:slug-ad: das-administration

[#admin-dates]
== {team} dates

|===
| |Dates |Status

|{team} Review Period 1
|10/22 - 11/04
|Done

|{team} Review Period 2
|12/03 - 12/09
|Done
|===

{team} is part of the DAS component. For all DAS review, approval, and publishing dates, see xref:das.adoc#key-dates[DAS key dates].

[#admin-locations]
== {team} locations

GitLab repository containing the migrated {team} content:: https://gitlab.com/gadhs/pamms-sources/{slug-ad}

Issue tracker for filing {team} migration tickets:: https://gitlab.com/gadhs/pamms-sources/{slug-ad}/-/issues

Published {team} content on PAMMS migration staging site:: https://gadhs.gitlab.io/pamms-sources/migration-staging/das/administration/5600/

For all DAS repositories and issue tracker URLs, see xref:das.adoc#key-locations[DAS key locations]

include::page$das.adoc[tag=issue]

== Migration items of note

=== Migration [.line-through]#pending# complete

The following appendices have been migrated and are ready for review as of 10/28/2024. (They were not available for review at the beginning of review period 1.)

* Appendix H
* Appendix I
* Appendix J
* Appendix L

// 9015 has a table caption count and reference