= ORS Review Instructions
:team: ORS
:slug: ors

== Key Dates

|===
| |Date(s) |Status

|Review Period 1
|5/16 - 5/22
|Done

|Review Period 2
|5/23 - 5/29
|Done

|{team} Approval for Promotion to Production Deadline
|5/30
|Done

|Roll out {team} component to PAMMS (if approved)
|5/31
|Done
|===

== Key Locations

GitLab repository containing the migrated {team} content:: https://gitlab.com/gadhs/pamms-sources/{slug}

Issue tracker for filing {team} migration tickets:: https://gitlab.com/gadhs/pamms-sources/{slug}/-/issues

Published {team} content (webpages and PDFs) on PAMMS migration staging site:: https://gadhs.gitlab.io/pamms-sources/migration-staging/{slug}/
+
--
The PDF for a manual or a policy can be accessed from any manual page or a policy page, respectively, by clicking on "PDF", which is displayed next to "Edit this Page" in the breadcrumb bar at the top of a page.

image::ors-pdf-link.png[]
--

== {team} repository overview

The {team} repository in the PAMMS Sources group contains the {team} content that was migrated to AsciiDoc. This is the repository where the {team} writers and publishers will update, add, and manage their content in the future. This repository's issue tracker is also where the {team} team members will file tickets regarding any errors they see in the content during the review periods.

=== Important files and directories in the repository

You will see the following files and folders in the {team} content source repository located at https://gitlab.com/gadhs/pamms-sources/{slug}.

antora.yml:: This file provides important information to both IntelliJ and Antora, including the component name, title, version, and registered navigation files. Changing the value of the `name` or `version` key in this file has serious consequences that impact all the agency's URLs in PAMMS. This file cannot be moved and its filename cannot be changed.
+
--
For {team}, the following values have been assigned to the keys in _antora.yml_:

[,yml]
----
name: ors <1>
title: Office of Refugee Services <2>
version: ~ <3>
asciidoc:
  attributes:
    manual-number: MAN3180@ <4>
    manual-title: Refugee Support Services@
nav: <5>
- modules/ROOT/nav.adoc
----
<1> Component name. This value is used in component-to-component xrefs and in the URL of the PAMMS site. Changing this value will change the URLs of all of an agency's pages and assets in the PAMMS site.
<2> Component title. This value is displayed in the component drawer, component navigation, and breadcrumbs.
<3> Version of the component. The content of {team} isn't versioned, therefore the version is set to `~`, which effectively sets the version to "no version". Changing this value will change the URLs of all of an agency's pages and assets in the PAMMS site.
<4> AsciiDoc attributes that are assigned to all or almost all the pages that belong to a component can be set in _antora.yml_. Here we've created an attribute name `manual-number` and assigned it the value `MAN3180`. This attribute will be applied to all pages that belong to the manual (i.e., every file but policy 1300). The `@` at the end of the value indicates that the value is *soft set* which means the attribute can be overridden from any page. The `@` won't be displayed when the attribute is referenced.
<5> Navigation files are registered under the `nav` key. If a navigation file isn't registered Antora will ignore it. {team} only has one navigation file, located in the _ROOT_ folder.
--

modules folder:: This folder is required by Antora. It should not be moved and its name can't be changed.

ROOT folder:: This folder must be a direct child of the _modules_ folder. It's name and capitalization signals to Antora that the folders and files within it should be handled in a particular way. If this folder's name is changed it has significant impacts to the agency's URLs in PAMMS. Antora assigns all files it finds in this folder to the `ROOT` module.

pages folder:: This folder is a direct child of a module folder (in {team}, the only module folder is _ROOT_). Antora applies actions to the files in the _pages_ folder automatically. This name can't be changed. Antora assigns all the files it finds in this folder to the `pages` family.

{dot}adoc files in a pages folder:::: Pages are files stored in a _pages_ folder in a module folder. Each file in _pages_ represents an HTML page in the PAMMS site. Page files must end with the _.adoc_ file extension.
+
--
The filename of a page file, e.g., _general-information.adoc_, _gateway-procedures.adoc_, etc., is used in xrefs to make links between pages and to create navigation links. The filename, minus the _.adoc_ file extension is used in the URL of the page in the PAMMS site.

Changing the filename of a file in the _pages_ folder will change its URL in the PAMMS site, potentially leading to broken external links. It will also break xrefs and includes within the PAMMS site if the filename isn't updated correctly using the menu:Refactor[Rename] feature in IntelliJ. *Do not* use spaces, capital letters or underscores in filenames (or folder names); *do* use hyphens to separate words in filenames.

Page files must have a page title. Page files are published to the PAMMS site whether they're listed in a navigation file or not (so don't store files you don't want published in _pages_).
--

images folder:: This folder, which is only present in a module folder if image files (.png, .jpg, .gif) are present, is a direct child of a module folder. Antora applies default actions to the files in an _images_ folder automatically. This folder's name can't be changed. Antora assigns all files it finds in this folder to the `images` family.
+
--
Changing the filename of a file in the _images_ folder will change its URL in the PAMMS site, potentially leading to broken external links. It will also break image macros within the PAMMS site if the filename isn't updated correctly using the menu:Refactor[Rename] feature in IntelliJ. *Do not* use spaces, capital letters or underscores in filenames (or folder names); *do* use hyphens to separate words in filenames.

Image files are published to the PAMMS site whether they're referenced using an image macro or not (so don't store files you don't want published in _images_).
--

nav.adoc / navigation files:: The navigation file(s) for a module is stored as a direct child of the module folder. This file is typical named _nav.adoc_ but it can have a custom named. It must use the _.adoc_ file extension. A navigation file contains a list of xrefs of the pages in that module or component. A module doesn't have to have a navigation file or it can have multiple navigation files; however, for the navigation list (or lists) contained in the file(s) to be published to the site, the navigation file must be registered in _antora.yml_.

== Items to Review

Specific attention should be paid to the following items during the review periods:

=== Policy 1300

. xref:ors::1300.adoc[Policy 1300: filename, policy title, and page title]
+
We followed the pattern used by agencies in the pilot project and simply used the number of the policy as the filename, i,e., 1300.adoc.
This filename is used to create xrefs to the policy from other pages and shows up in the URL of the page.
Unlike the pilot project agencies, we weren't sure what the official policy title was, and therefore used the folder name from Sharepoint.
Again, following the pattern used by the pilot project agencies, the policy # + policy title were used to create a page title, which is also the default title used in the navigation sidebar on the left.
If you want the filename, policy title, or page title to be changed, file an issue with what you'd like them to be.

. xref:ors::1300.adoc[Policy 1300: position in sidebar navigation]
We inserted the policy at the top of the navigation, however, we can move to the bottom if you prefer.

. Policy 1300: metadata and display of metadata
+
--
The page header of Policy 1300 contains the following attributes:

[,asciidoc]
----
= 1300 Repatriation
:policy-number: 1300
:policy-title: Repatriation
:revision-date: 02/01/22
:next-review: 02/01/25
:!manual-number:
:!manual-title:
----

The policy title wasn't explicitly stated in the original file we were given. Is this its title, or should we remove this attribute?
--

=== Manual 3180

. Filenames of the pages in Manual 3180
+
Manual 3180 was broken up into individual using the manual's TOC and headings in the manual that were in all UPPERCASE. Unlike the pages in the manuals of the pilot agencies, there aren't any policy numbers per top-level TOC item. Therefore we used the key words / unique snippet of each applicable TOC heading to create the filenames. These filenames show up in the URLs of the pages. We didn't use the full TOC heading of each section heading as the filename because some of these headings were very long which would make the URL long and make referencing the page using an xref cumbersome. If you would like any of these filenames changed, file an issue with what you'd like them to be.

. Page: xref:ors::glossary.adoc[Glossary of Terms and Abbreviations]
+
--
This page is structured using the default AsciiDoc description list, which is also the structure AsciiDoc uses for semantic glossary sections. By default, this means that when published, the description item/term is displayed using the bold and italic styling and its description is indented on the following line.
Child items/terms and other child list items are further indented to nest them under their parent item. This style has also been used for the terms under the xref:ors::1300.adoc#definitions[Definitions section in Policy 1300].

Alternatively, we can apply the `horizontal` and `label-width` options to the description list, and then they will display like the Step lists in the pilot agencies. In that case, the items would be displayed like:

[horizontal,label-width=10]
Afghan Humanitarian Parolee (AHP):: A person from Afghanistan who has been granted  temporary permission from the United Sates government to enter and physically stay in the  country for a period of time, usually two years.

Afghan or Iraqi Special Immigrant:: A person from Afghanistan or Iraq arriving in the United States after employment with the United States Military or United Sates based agency in Afghanistan or Iraq.

Alien:: A person who is not a citizen of the United States.
--

. Page: xref:ors::general-eligibility-requirements.adoc[General Eligibility Requirements]
+
Carefully review the tables on this page to make sure the content is in the correct columns and no content is missing.

. Page: xref:ors::documentation-requirements.adoc[Documentation Requirements]
.. Carefully review the tables on this page to make sure the content is in the correct columns and no content is missing.
.. We added the text "(see Note below table)" in place of `+*+`, `+**+`, and `+***+` because sequences of asterisks have special meaning in AsciiDoc (they indicate that text should be bold). Such text was used in one of the tables that has three notes so we replicated it across all asterisk markers, assigned them an id, and created deeplinks between the notes and the previous asterisk markers.
.. In the original document provided to us, the lower alpha list markers under Chart 4b - 4d didn't match the letter in the Chart title. We adjusted this so that "b" is under Chart 4b, and so forth.

. Page: xref:ors::verifying-immigrant-status.adoc[Descriptions and Examples of Cards, Forms, and Letters Verifying Immigrant Status]
.. In the original document given to us, the lines "Sincerely,
Typed Name
Title
Enclosure(s): I-94 Card(s)" were located at the end of the Asylum Benefits section. Upon review during the migration, we think they were meant to be at the end of the "Example of an Asylum Approval Letter", i.e., list item #9 on xref:ors::verifying-immigrant-status.adoc[Descriptions and Examples of Cards, Forms, and Letters Verifying Immigrant Status]. If this isn't were this text belongs, please file an issue.
.. There are several references, such as "See page 160-4 for an example of an Asylum Approval Letter" and "See page 160-3 for an example of the Certification Letter". We can create links to these examples and remove the page number (as those won't be relevant in the website, nor in the generated PDF) if you want us to.
. Page: xref:ors::contract-agencies-and-services.adoc[Refugee Program Contract Agencies and Services]
.. This page has been converted into AsciiDoc. Carefully review the tables on this page to make sure the content is in the correct columns and no content is missing.
.. We added a section heading above each table so that they can be easily browsed in the "Content" sidebar on the right. If you want these headings removed or changed, file an issue.

== How to file an issue for a change or error

If you identify an error, would like a change, or have a question, please file an issue in the {team} content repository.

. Go to the repository issue tracker located at https://gitlab.com/gadhs/pamms-sources/{slug}/-/issues.
. Click on the "New Issue" button.
. In the "Title" field provide a succinct subject line.
. In the "Description" field provide:
.. The URL(s) of the page(s) or filename of the file on which you discovered the problem, have a question about, or have feedback about.
.. Your question, feedback, or a description of the problem.
.. A brief statement of what you expected to see or the behavior you expected (if applicable)
.. You can attach screenshots using the paperclip icon in the toolbar about the Description field if it’s easier to show the problem rather than describe it.

We'll respond with a day or two during the business week to let you know how we can address the problem or provide and answer to your question.