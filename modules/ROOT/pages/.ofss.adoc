= OFSS Review Instructions
:team: OFSS
:slug: ofss

== Key Dates

|===
| |Date(s) |Status

|Review Period 1 (Policies Only)
|5/29 - 6/04
|Done

|Review Period 2 (MAN1425)
|6/14 - 6/21
|Done

|Review Period 3
|6/27 - 7/03
|Done

|Review Period 4
|7/11 - 7/15
|Done

|{team} Approval for Promotion to Production Deadline
|7/15 EOD
|Done

|Roll out {team} component to PAMMS (if approved)
|7/16
|Done
|===

== Key Locations

GitLab repository containing the migrated {team} content:: https://gitlab.com/gadhs/pamms-sources/{slug}

Issue tracker for filing {team} migration tickets:: https://gitlab.com/gadhs/pamms-sources/{slug}/-/issues

Published {team} content (webpages and PDFs) on PAMMS migration staging site:: https://gadhs.gitlab.io/pamms-sources/migration-staging/{slug}/
+
--
The PDF for MAN1425 can be accessed from any page in the manual by clicking on "PDF", which is displayed next to "Edit this Page" in the breadcrumb bar at the top of a page.
--

== {team} repository overview

The {team} repository in the PAMMS Sources group contains the {team} content that was migrated to AsciiDoc. This is the repository where the {team} writers and publishers will update, add, and manage their content in the future. This repository's issue tracker is also where the {team} team members will file tickets regarding any errors they see in the content during the review periods.

=== Important files and directories in the repository

You will see the following files and folders in the {team} content source repository located at https://gitlab.com/gadhs/pamms-sources/{slug}.

antora.yml:: This file provides important information to both IntelliJ and Antora, including the component name, title, version, and registered navigation files. Changing the value of the `name` or `version` key in this file has serious consequences that impact all of {team}'s URLs in PAMMS. This file cannot be moved and its filename cannot be changed.
+
--
For {team}, the following values have been assigned to the keys in _antora.yml_:

[,yml]
----
name: ofss <1>
version: ~ <2>
title: Office of Facilities and Support Services <3>
asciidoc:
  attributes:
    manual-number: MAN1425@ <4>
    manual-title: Coordinated Transportation System Transportation Manual@
    revision-date: July 1, 2023@
    next-review: July 1, 2025@
nav: <5>
- modules/ROOT/nav.adoc
- modules/man1425/nav.adoc
----
<1> Component name. This value is used in component-to-component xrefs and in the URL of the PAMMS site. Changing this value will change the URLs of all of {team}'s pages and assets in the PAMMS site.
<2> Version of the component. The content of {team} isn't versioned, therefore the version is set to `~`, which effectively sets the version to "no version". Changing this value will change the URLs of all of {team}'s pages and assets in the PAMMS site.
<3> Component title. This value is displayed in the component drawer, component navigation, and breadcrumbs.
<4> AsciiDoc attributes that are assigned to all pages that belong to a component can be set in _antora.yml_. We've created several custom attributes, such as an attribute name `manual-number` and assigned it the value `MAN1425`. These attributes will be applied to all pages in the component (except for the policy files in which `manual-number` and `manual-title` are unset). These attributes are available for display on all pages in the component using attribute references. The `@` at the end of the value indicates that the value is *soft set* which means the attribute can be overridden from any page (as they are in the policy files). The `@` won't be displayed when the attribute is referenced.
<5> Navigation files are registered under the `nav` key. If a navigation file isn't registered in the component's _antora.yml_ file, Antora will ignore it (i.e., not use it). {team} has a navigation file per module folder. Antora combines these navigation files when it builds the PAMMS site to create the component navigation menu on the left side of PAMMS site.
--

modules folder:: This folder is required by Antora. It should not be moved and its name can't be changed.

ROOT folder:: This folder must be a direct child of the _modules_ folder. It's name and capitalization signals to Antora that the folders and files within it should be handled in a particular way. If this folder's name is changed it has significant impacts to {team}'s URLs in PAMMS. Antora assigns all files it finds in this folder to the `ROOT` module.

man1425 folder:: This folder is a custom module folder and must be a direct child of the _modules_ folder. It's location signals to Antora that the folders and files within it should be handled in a particular way. If this folder's name is changed it has significant impacts to {team}'s URLs in PAMMS. Antora assigns all files it finds in this folder to the `man1425` module.

pages folder:: This folder is a direct child of a module folder. Antora applies actions to the files in the _pages_ folder automatically. This name can't be changed. Antora assigns all the files it finds in this folder to the `pages` family.

{dot}adoc files in a pages folder:::: Pages are files stored in a _pages_ folder in a module folder. Each file in _pages_ represents an HTML page in the PAMMS site. Page files must end with the _.adoc_ file extension.
+
--
The filename of a page file, e.g., _1410.adoc_, _coordinated-transportation.adoc_. etc., is used in xrefs to make links between pages and to create navigation links. The filename, minus the _.adoc_ file extension is used in the URL of the page in the PAMMS site.

Changing the filename of a file in the _pages_ folder will change its URL in the PAMMS site, potentially leading to broken external links. It will also break xrefs and includes within the PAMMS site if the filename isn't updated correctly using the menu:Refactor[Rename] feature in IntelliJ. *Do not* use spaces, capital letters or underscores in filenames (or folder names); *do* use hyphens to separate words in filenames.

Page files must have a page title. Page files are published to the PAMMS site whether they're listed in a navigation file or not (so don't store files you don't want published in _pages_).
--

images folder:: This folder, which is only present in a module folder if image files (.png, .jpg, .gif) are present, is a direct child of a module folder. Antora applies default actions to the files in an _images_ folder automatically. This folder's name can't be changed. Antora assigns all files it finds in this folder to the `images` family.
+
--
Changing the filename of a file in the _images_ folder will change its URL in the PAMMS site, potentially leading to broken external links. It will also break image macros within the PAMMS site if the filename isn't updated correctly using the menu:Refactor[Rename] feature in IntelliJ. *Do not* use spaces, capital letters or underscores in filenames (or folder names); *do* use hyphens to separate words in filenames.

Image files are published to the PAMMS site whether they're referenced using an image macro or not (so don't store files you don't want published in _images_).
--

nav.adoc / navigation files:: The navigation file(s) for a module is stored as a direct child of the module folder. This file is typical named _nav.adoc_ but it can have a custom filename. It must use the _.adoc_ file extension. A navigation file contains a list of xrefs of the pages in that module or component. A module doesn't have to have a navigation file or it can have multiple navigation files; however, for the navigation list (or lists) contained in the file(s) to be published to the site, the navigation file must be registered in _antora.yml_.

== Items to Review
////
. Topic folders
+
--
To keep the pages of each chapter, appendices, and exhibits organized, we created topic folders within the _man1425_ module folder. We chose the chapter name, instead of its number, for each folder name, since these are displayed in the URLs and "Chapter 1", "Chapter 2" may not be very useful when people are searching for a topic.

Please review the topic folder names in GitLab at: https://gitlab.com/gadhs/pamms-sources/ofss/-/tree/main/modules/man1425/pages?ref_type=heads

A topic folder's name + filename is used to create xrefs to other pages that aren't in the same folder and they show up in the URL of a page that belongs to that chapter, appendix, or exhibit.
--

. Filenames (except for _index.adoc_ files)
+
--
For Man1425, each major section became a page. The title of each section was extracted to become the filename, i.e., "Contractor Lifecyle" became _contractor-lifecycle.adoc_.

Please review the filenames in within https://gitlab.com/gadhs/pamms-sources/ofss/-/tree/main/modules/man1425/pages?ref_type=heads[each topic folder in GitLab]
This filename is used to create xrefs to other pages and shows up in the URL of the page.

If you prefer that the filename be the policy title instead of its number, e.g., _records-management.adoc_, _coordinated-transportation-system.adoc_, etc., <<file-issue,file an issue>> and let us know the preferred policy filename pattern.
--

. _index.adoc_ files
+
--
Each topic folder contains a file named _index.adoc_. This file contains the content that was included in the "Introduction" of each chapter. This is a best practice / convention used by the web so that if someone were to only type in the URL to a chapter, e.g, https://gadhs.gitlab.io/pamms-sources/migration-staging/ofss/man1425/coordinated-transportation/, they don't get a 404 error or Page not Found. In essence, an _index.adoc_ file becomes the "start page" of a folder
--

. Section titles and Steps
+
--
In the original MAN1425, many of the headings were also list items, e.g., "1. How Contractors Are Selected", "B. Rider and Trip Type Eligibility Determination". In AsciiDoc, a chunk of content can be a heading/section title or a list item. It can't be both. Therefore, we dropped the list marker and made each chunk of content section heading. Section headings are automatically displayed on the right side of a page under "Contents".

The exception to this were the "Step" items which were typically Heading 5's in the Word document. We converted these to description lists.
--
////

. The images under xref:ofss:man1425:coordinated-transportation/contractor-lifecycle.adoc#process-flow-contractor-selection-renewals-and-intergovernmental-agreements[Process Flow – Contractor Selection – Renewals and Intergovernmental Agreements Process] have been fixed.

. The images under xref:ofss:man1425:vehicle-management/vehicle-lifecycle.adoc#process-flow-acquisition-via-purchase[Process Flow – Acquisition via Purchase] have been fixed.

[#file-issue]
== How to file an issue for a change or error

If you identify an error, would like a change, or have a question, please file an issue in the {team} content repository.

. Go to the repository issue tracker located at https://gitlab.com/gadhs/pamms-sources/{slug}/-/issues.
. Click on the "New Issue" button.
. In the "Title" field provide a succinct subject line.
. In the "Description" field provide:
.. The URL(s) of the page(s) or filename of the file on which you discovered the problem, have a question about, or have feedback about.
.. Your question, feedback, or a description of the problem.
.. A brief statement of what you expected to see or the behavior you expected (if applicable)
.. You can attach screenshots using the paperclip icon in the toolbar about the "Description" field if it’s easier to show the problem rather than describe it.

We'll respond with a day or two during the business week to let you know how we can address the problem or provide and answer to your question.